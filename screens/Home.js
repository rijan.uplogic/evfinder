import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  ScrollView,
  Platform,
  TouchableOpacity,
  Modal,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {useEffect, useState, useRef} from 'react';
import MapView from 'react-native-maps/lib/MapView';
import {PROVIDER_GOOGLE} from 'react-native-maps/lib/ProviderConstants';
import Data from '../utils/Data.json';
import {MapMarker} from 'react-native-maps/lib/MapMarker';
import Geolocation from '@react-native-community/geolocation';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import ViewShot from 'react-native-view-shot';

const screenheight = Dimensions.get('screen').height;
const screenwidth = Dimensions.get('screen').width;

const Home = () => {
  const ref = useRef();
  const [load, setLoad] = useState(true);
  const [lat, setLat] = useState('');
  const [lng, setLng] = useState('');
  const [img, setImg] = useState('');
  const [mod, setMod] = useState(false);
  useEffect(() => {
    const loc = check_location_access();
  }, []);
  const check_location_access = async () => {
    console.log('access check');
    await RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 10,
      fastInterval: 50,
    })
      .then(async data => {
        console.log('data', data);
        if (data) {
          await getlocation();
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  const getlocation = () => {
    console.log('access');
    Geolocation.getCurrentPosition(
      position => {
        console.log('position', position);
        // await AsyncStorage.setItem('from_screen', "Splashscreen");
        // await this.setState({lat: position.coords.latitude});
        setLat(position.coords.latitude);
        setLng(position.coords.longitude);
        setLoad(false);
        // await this.setState({lng: position.coords.longitude});
      },
      async error => {
        console.log('err', error);
        // await this.setState({showIndicator: false});
        Geolocation.requestAuthorization();
        var def_lat = '9.920173';
        var def_lng = '78.092606';
        var def_country = 'india';
        setLat(def_lat);
        setLoad(false);
        setLng(def_lng);
        // await this.setState({lat: def_lat});
        // await this.setState({lng: def_lng});
        global.cus_lat = def_lat;
        global.cus_lng = def_lng;
      },
      {enableHighAccuracy: false, timeout: 30000},
    );
  };
  const takeScreenShot = () => {
    ref.current.capture().then(uri => {
      console.log(uri);
      setImg(uri);
      setMod(true);
      upload(uri);
    });
  };
  const upload = uri => {
    fetch('http://3.7.20.173:8503/api/upload/', {
      method: 'POST',
      headers: {
        Authorization: 'Basic YWRtaW46MTIzNDU2',
      },
      body: {
        file: uri,
      },
    })
      .then(res => res.json())
      .then(res => console.log(res))
      .catch(err => console.log(err, 'error'));
  };

  if (load) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={{fontSize: 30, color: '#03fca1'}}>
          Getting stations nearby !!
        </Text>
      </View>
    );
  } else {
    return (
      <View style={{flex: 1}}>
        {lat !== '' && lng !== '' && (
          <ViewShot
            ref={ref}
            options={{
              fileName: 'file-name', // screenshot image name
              format: 'webp', // image extention
              quality: 0.9, // image quality
            }}>
            <MapView
              provider={PROVIDER_GOOGLE}
              initialRegion={{
                latitude: Number(lat),
                longitude: Number(lng),
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
              }}
              style={{height: '100%', width: '100%'}}>
              {Data.map((val, index) => {
                return (
                  <MapMarker
                    coordinate={{
                      latitude: Number(val.latitude),
                      longitude: Number(val.longitude),
                    }}
                    key={index}>
                    <View
                      style={{
                        height: 20,
                        width: 20,
                        backgroundColor: '#03fca1',
                        borderRadius: 10,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={{color: 'black'}}>{index}</Text>
                    </View>
                  </MapMarker>
                );
              })}
              {lat !== '' && lng !== '' && (
                <MapMarker
                  coordinate={{latitude: Number(lat), longitude: Number(lng)}}>
                  <View
                    style={{
                      height: screenheight * 0.08,
                      width: screenheight * 0.08,
                      borderRadius: screenheight * 0.04,
                      backgroundColor: 'rgba(255,0,0,0.5)',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{
                        height: screenheight * 0.04,
                        width: screenheight * 0.04,
                        borderRadius: screenheight * 0.02,
                        backgroundColor: 'red',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Image
                        source={require('../utils/loc.png')}
                        style={{
                          height: screenheight * 0.02,
                          width: screenheight * 0.02,
                          tintColor: 'white',
                        }}
                      />
                    </View>
                  </View>
                </MapMarker>
              )}
            </MapView>
          </ViewShot>
        )}
        {lat !== '' && lng !== '' && (
          <View
            style={{
              position: 'absolute',
              alignItems: 'center',
              justifyContent: 'space-between',
              top: screenheight * 0.01,
              flexDirection: 'row',
              height: screenheight * 0.1,
              width: '92%',
              marginHorizontal: '4%',
            }}>
            <View style={{width: '15%'}}>
              <Image
                source={require('../utils/menu.png')}
                style={{
                  height: screenheight * 0.03,
                  width: screenheight * 0.03,
                }}
              />
            </View>
            <View
              style={{
                width: '85%',
                backgroundColor: 'black',
                borderRadius: screenheight * 0.01,
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: '4%',
                justifyContent: 'space-between',
              }}>
              <Image
                source={require('../utils/record.png')}
                style={{height: 10, width: 10}}
              />
              <TextInput
                style={{width: '80%'}}
                placeholder="Search for the compatible chargers"
                placeholderTextColor="white"
              />
              <Image
                source={require('../utils/panel.png')}
                style={{height: 20, width: 20}}
              />
            </View>
          </View>
        )}
        {lat !== '' && lng !== '' && (
          <ScrollView
            horizontal
            style={{
              height: screenheight * 0.35,
              width: screenwidth,
              position: 'absolute',
              bottom: screenheight * 0.01,
            }}>
            {Data.map((val, index) => {
              return (
                <View
                  style={{
                    height: '100%',
                    backgroundColor: 'black',
                    width: screenwidth * 0.6,
                    marginLeft: screenwidth * 0.05,
                    borderRadius: screenheight * 0.02,
                    padding: '2%',
                    justifyContent: 'space-between',
                  }}
                  key={index}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <View>
                      <Text style={{color: 'white'}} numberOfLines={1}>
                        {val.name.toUpperCase()}
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={{fontSize: 12}} numberOfLines={1}>
                          {val.address}
                        </Text>
                        <Text
                          style={{fontSize: 12, color: 'red'}}
                          numberOfLines={1}>
                          {(val.distance / 1000).toFixed(1)} Km
                        </Text>
                      </View>
                    </View>
                    <Image
                      source={require('../utils/navigation.png')}
                      style={{height: 25, width: 25}}
                    />
                  </View>
                  <Text style={{fontSize: 12, color: '#03fca1'}}>
                    SUPPORTED CONNECTIONS
                  </Text>
                  {val.connector_types.map((a, indice) => {
                    return (
                      <View
                        key={indice}
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            // justifyContent: 'space-between',
                            width: '80%',
                          }}>
                          <Image
                            source={require('../utils/port.png')}
                            style={{height: 25, width: 25, tintColor: 'white'}}
                          />
                          <View style={{paddingLeft: '4%'}}>
                            <Text
                              style={{
                                color: 'white',
                                opacity: a.split('-')[0] == 'lvl1dc' ? 1 : 0.7,
                              }}>
                              {a.split('-')[0] == 'lvl1dc'
                                ? 'Level 1 Dc'
                                : a.split('-')[0] == 'lvl2dc'
                                ? 'Level 2 Dc'
                                : 'Normal AC'}
                            </Text>
                            <Text
                              style={{
                                color: '#03fca1',
                                opacity: a.split('-')[0] == 'lvl1dc' ? 1 : 0.7,
                                fontSize: 10,
                              }}>
                              {a.split('-')[0] == 'lvl1dc'
                                ? '15kW Fast Charging'
                                : a.split('-')[0] == 'lvl2dc'
                                ? '50kW Fast Charging'
                                : '3kW Charging'}
                            </Text>
                          </View>
                        </View>
                        <Text style={{color: 'white'}}>x{a.split('-')[1]}</Text>
                      </View>
                    );
                  })}
                  <View
                    style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Image
                      source={require('../utils/down.png')}
                      style={{height: 25, width: 25, tintColor: 'white'}}
                    />
                  </View>
                </View>
              );
            })}
          </ScrollView>
        )}
        <TouchableOpacity
          onPress={() => {
            takeScreenShot();
          }}
          style={{
            height: screenheight * 0.05,
            width: screenheight * 0.05,
            // backgroundColor: '#03fca1',
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
            bottom: screenheight * 0.4,
            right: screenwidth * 0.02,
          }}>
          <Image
            source={require('../utils/screenshot.png')}
            style={{height: 50, width: 50}}
          />
        </TouchableOpacity>
        <Modal visible={mod} transparent onRequestClose={() => setMod(false)}>
          <TouchableWithoutFeedback onPress={() => setMod(false)}>
            <View
              style={{
                height: screenheight * 0.3,
                width: screenwidth,
                alignItems: 'flex-end',
                justifyContent: 'center',
                paddingHorizontal: '4%',
              }}>
              <ImageBackground
                source={{uri: img}}
                style={{height: screenheight * 0.2, width: screenwidth * 0.25}}>
                <View
                  style={{
                    height: '100%',
                    width: '100%',
                    backgroundColor: 'rgba(0,0,0,0.5)',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image
                    source={require('../utils/check.png')}
                    style={{height: 50, width: 50}}
                  />
                </View>
              </ImageBackground>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  }
};

export default Home;

const styles = StyleSheet.create({});
