/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {StyleSheet} from 'react-native';
import Home from './screens/Home';
import {enableLatestRenderer} from 'react-native-maps';

enableLatestRenderer();

function App() {
  return <Home />;
}

const styles = StyleSheet.create({});

export default App;
